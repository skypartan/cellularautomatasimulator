#pragma once

#define OLC_PGE_APPLICATION

#include "olcPixelGameEngine.h"

class olcMain : public olc::PixelGameEngine
{
private:
	int** cells;

public:
	olcMain();

	bool OnUserCreate() override;
	bool OnUserUpdate(float fElapsedTime) override;
};
