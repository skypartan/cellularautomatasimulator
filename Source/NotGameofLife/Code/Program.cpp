#include "Program.h"

olcMain::olcMain()
{
	sAppName = "Not Game of Life";
}

bool olcMain::OnUserCreate()
{
	cells = (int**) calloc(ScreenHeight(), sizeof(int*));
	for (int i = 0; i < ScreenHeight(); i++)
		cells[i] = (int*) calloc(ScreenWidth(), sizeof(int));


	for (int x = 0; x < 10; x++)
		for (int y = 0; y < 10; y++)
			cells[y][x] = rand() % 2;

	return true;
}

bool olcMain::OnUserUpdate(float fElapsedTime)
{
	for (int x = 1; x < ScreenWidth() - 1; x++)
	{
		for (int y = 1; y < ScreenHeight() - 1; y++)
		{
			int n_neightbors = 0;
			n_neightbors += cells[y - 1][x - 1] + cells[y - 1][x] + cells[y - 1][x + 1]; // Linha superior
			n_neightbors += cells[y][x - 1] + 0 + cells[y][x + 1]; // Linha atual
			n_neightbors += cells[y + 1][x - 1] + cells[y + 1][x] + cells[y + 1][x + 1]; // Linha inferior


			if (cells[y][x] == 1)
				if (n_neightbors == 2 || n_neightbors == 3)
					cells[y][x] = 1;
				else
					cells[y][x] = 0;
			else
				if (n_neightbors == 3)
					cells[y][x] = 1;
				else
					cells[y][x] = 0;


			if (cells[y][x] == 1)
				Draw(x, y, olc::Pixel(255, 255, 255));
			else
				Draw(x, y, olc::Pixel(0, 0, 0));
		}
	}

	return true;
}

int main()
{
	olcMain program;
	if (program.Construct(256, 240, 4, 4))
		program.Start();

	return EXIT_SUCCESS;
}
