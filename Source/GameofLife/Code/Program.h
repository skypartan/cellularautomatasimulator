#pragma once

#define OLC_PGE_APPLICATION

#include "olcPixelGameEngine.h"

class olcMain : public olc::PixelGameEngine
{
private:
	int* m_state;
	int* m_output;

	bool m_drawMode;

public:
	olcMain();

	bool OnUserCreate() override;
	bool OnUserUpdate(float fElapsedTime) override;
	bool OnUserDestroy() override;

	int GetCell(int x, int y);
	void SetCell(int x, int y, std::string data);
};
