#include "Program.h"

/*
	Comandos:
		espa�o: Alterar entre modo de desenho e modo de opera��o
		mouse1: Desenhar c�lulas em modo de desenho
*/

using namespace std;

olcMain::olcMain()
{
	sAppName = "Game of Life";
}

bool olcMain::OnUserCreate()
{
	m_state = new int[ScreenWidth() * ScreenHeight()];
	memset(m_state, 0, ScreenWidth() * ScreenHeight() * sizeof (int));

	m_output = new int[ScreenWidth() * ScreenHeight()];
	memset(m_output, 0, ScreenWidth() * ScreenHeight() * sizeof (int));

	// Inicializando c�lulas

	// Aleat�rio
	//for (int i = 0; i < ScreenWidth() * ScreenHeight(); i++)
	//	m_state[i] = rand() % 2;

	// R-Pentonimo
	//SetCell(80, 50, ".##");
	//SetCell(80, 51, "##.");
	//SetCell(80, 52, ".#.");

	// Glider gun
	SetCell(60, 45, "........................#............");
	SetCell(60, 46, "......................#.#............");
	SetCell(60, 47, "............##......##............##.");
	SetCell(60, 48, "...........#...#....##............##.");
	SetCell(60, 49, "##........#.....#...##...............");
	SetCell(60, 50, "##........#...#.##....#.#............");
	SetCell(60, 51, "..........#.....#.......#............");
	SetCell(60, 52, "...........#...#.....................");
	SetCell(60, 53, "............##.......................");

	//
	return true;
}

bool olcMain::OnUserUpdate(float fElapsedTime)
{
	if (GetKey(olc::SPACE).bPressed)
		m_drawMode = !m_drawMode;

	this_thread::sleep_for(25ms);

	// Armazenar estado de sa�da
	for (int i = 0; i < ScreenWidth() * ScreenHeight(); i++)
		m_output[i] = m_state[i];

	if (!m_drawMode)
	{
		for (int x = 0; x < ScreenWidth(); x++)
		{
			Draw(x, ScreenHeight() - 1, olc::Pixel(254, 106, 2));
		}

		// Operar c�lulas
		for (int x = 0; x < ScreenWidth() - 1; x++)
		{
			for (int y = 0; y < ScreenHeight() - 1; y++)
			{
				int nNeightbors = GetCell(x - 1, y - 1) + GetCell(x, y - 1) + GetCell(x + 1, y - 1) +
					GetCell(x - 1, y) + 0 + GetCell(x + 1, y) +
					GetCell(x - 1, y + 1) + GetCell(x, y + 1) + GetCell(x + 1, y + 1);

				// TODO: Remover convers�o de booleano para inteiro
				if (GetCell(x, y) == 1)
					m_state[y * ScreenWidth() + x] = nNeightbors == 2 || nNeightbors == 3;
				else
					m_state[y * ScreenWidth() + x] = nNeightbors == 3;

				if (GetCell(x, y) == 1)
					Draw(x, y, olc::Pixel(255, 255, 255));
				else
					Draw(x, y, olc::Pixel(0, 0, 0));
			}
		}
	}
	else
	{
		for (int x = 0; x < ScreenWidth(); x++)
		{
			Draw(x, ScreenHeight() - 1, olc::Pixel(66, 134, 244));
		}

		if (GetMouse(0).bHeld)
		{
			int x = GetMouseX();
			int y = GetMouseY();

			m_state[y * ScreenWidth() + x] = m_state[y * ScreenWidth() + x] == 0 ? 1 : 0;

			if (GetCell(x, y) == 1)
				Draw(x, y, olc::Pixel(255, 255, 255));
			else
				Draw(x, y, olc::Pixel(0, 0, 0));
		}
	}

	//
	return true;
}

bool olcMain::OnUserDestroy()
{
	return true;
}

int olcMain::GetCell(int x, int y)
{
	return m_output[y * ScreenWidth() + x];
}

void olcMain::SetCell(int x, int y, string data)
{
	int i = 0;
	for (char c : data)
	{
		m_state[y * ScreenWidth() + x + i] = c == '#' ? 1 : 0;
		i++;
	}
}

int main()
{
	olcMain program;
	if (program.Construct(256, 240, 4, 4))
		program.Start();

	return EXIT_SUCCESS;
}
